
import os
import subprocess
import mimetypes
from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS
import tempfile
import reverse_geocode

def get_gps_data(image_path):
    with Image.open(image_path) as img:
        exif_data = img._getexif()
        if not exif_data:
            return None

        gps_info = {}
        for tag, value in exif_data.items():
            decoded = TAGS.get(tag, tag)
            if decoded == "GPSInfo":
                for gps_tag in value:
                    sub_decoded = GPSTAGS.get(gps_tag, gps_tag)
                    gps_info[sub_decoded] = value[gps_tag]

        return gps_info

def convert_to_decimal(degrees, minutes, seconds):
    return degrees + minutes / 60 + seconds / 3600

def apply_reverse_geocode(lat, lon):
    location = reverse_geocode.search(((lat, lon),))[0]
    return location['country'], location['city']

def convert_to_jpeg_if_needed(file_path):
    # Check if the file type is compatible with PIL
    mime_type, _ = mimetypes.guess_type(file_path)
    if mime_type in ["image/jpeg", "image/png", "image/gif", "image/bmp"]:
        return file_path  # No conversion needed

    else:
        with tempfile.NamedTemporaryFile(delete=False, suffix='.jpg') as temp_file:
            subprocess.run(['magick', 'convert', file_path, temp_file.name], check=True)
            return temp_file.name

    raise ValueError("Unsupported file type")

def get_image_location(file_path):
    try:
        jpeg_path = convert_to_jpeg_if_needed(file_path)
    except:
        return None

    try:
        gps_data = get_gps_data(jpeg_path)
        if gps_data:
            lat_ref = gps_data.get('GPSLatitudeRef')
            lon_ref = gps_data.get('GPSLongitudeRef')
            lat = gps_data.get('GPSLatitude')
            lon = gps_data.get('GPSLongitude')

            if lat and lon:
                lat_decimal = convert_to_decimal(*lat) * (-1 if lat_ref == 'S' else 1)
                lon_decimal = convert_to_decimal(*lon) * (-1 if lon_ref == 'W' else 1)

                # Get location information
                location = apply_reverse_geocode(lat_decimal, lon_decimal)
                return location
            else:
                return None
        else:
            return None
    finally:
        # Cleanup if a temporary file was created
        if jpeg_path != file_path:
            os.remove(jpeg_path)