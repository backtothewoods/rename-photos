from win32com.propsys import propsys, pscon
import datetime
import pytz
import os
import win32com.client
import re

vid_created_attribute_index = -1
image_created_attribute_index = -1

def get_image_created_attribute_index(file_path):
    global image_created_attribute_index
    if image_created_attribute_index >= 0:
        return image_created_attribute_index

    folder_path, _ = os.path.split(file_path)
    shell = win32com.client.Dispatch("Shell.Application")
    namespace = shell.NameSpace(folder_path)

    # Search for the attribute index
    for i in range(1000):  # Using a large number to cover all attributes
        attribute_name = namespace.GetDetailsOf(namespace.Items(), i)
        if "aufnahmedatum" in attribute_name.lower():  # Adjust the condition based on the exact name
            image_created_attribute_index = i
            return i

    return None  # Return None if the attribute is not found

def get_video_created_attribute_index(file_path):
    global vid_created_attribute_index
    if vid_created_attribute_index >= 0:
        return vid_created_attribute_index

    folder_path, _ = os.path.split(file_path)
    shell = win32com.client.Dispatch("Shell.Application")
    namespace = shell.NameSpace(folder_path)

    # Search for the attribute index
    for i in range(1000):  # Using a large number to cover all attributes
        attribute_name = namespace.GetDetailsOf(namespace.Items(), i)
        if "medium erstellt" in attribute_name.lower():  # Adjust the condition based on the exact name
            vid_created_attribute_index = i
            return i

    return None  # Return None if the attribute is not found

def clean_string(input_string):
    # Remove non-printable characters
    printable = set(chr(i) for i in range(32, 127))
    return ''.join(filter(lambda x: x in printable, input_string))

def get_image_date(file_path):
    folder_path, file_name = os.path.split(file_path)

    shell = win32com.client.Dispatch("Shell.Application")
    namespace = shell.NameSpace(folder_path)
    file = namespace.ParseName(file_name)

    date_taken_index = get_image_created_attribute_index(file_path)
    if date_taken_index:
        date_taken_str = namespace.GetDetailsOf(file, date_taken_index)
    else:
        date_taken_str = None

    if date_taken_str:
        # Clean the date string
        cleaned_date_str = clean_string(date_taken_str)

        # Use regex to extract date and time components
        match = re.search(r'(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})', cleaned_date_str)
        if match:
            day, month, year, hour, minute = match.groups()
            date_taken = datetime.datetime(int(year), int(month), int(day), int(hour), int(minute))
            return date_taken
        else:
            print(f"Cleaned Date format is not as expected: {cleaned_date_str}")
            return None
    else:
        return None

def get_video_date(file_path):
    folder_path, file_name = os.path.split(file_path)

    shell = win32com.client.Dispatch("Shell.Application")
    namespace = shell.NameSpace(folder_path)
    file = namespace.ParseName(file_name)

    date_taken_index = get_video_created_attribute_index(file_path)
    if date_taken_index:
        date_taken_str = namespace.GetDetailsOf(file, date_taken_index)
    else:
        date_taken_str = None

    if date_taken_str:
        # Clean the date string
        cleaned_date_str = clean_string(date_taken_str)

        # Use regex to extract date and time components
        match = re.search(r'(\d{2})\.(\d{2})\.(\d{4}) (\d{2}):(\d{2})', cleaned_date_str)
        if match:
            day, month, year, hour, minute = match.groups()
            date_taken = datetime.datetime(int(year), int(month), int(day), int(hour), int(minute))
            return date_taken
        else:
            print(f"Cleaned Date format is not as expected: {cleaned_date_str}")
            return None
    else:
        return None

"""
def get_video_date(file_path):
    properties = propsys.SHGetPropertyStoreFromParsingName(file_path)
    dt = properties.GetValue(pscon.PKEY_Media_DateEncoded).GetValue()

    if dt is None:
        return None  # Return None if the date is not available

    if not isinstance(dt, datetime.datetime):
        dt = datetime.datetime.fromtimestamp(int(dt))
        dt = dt.replace(tzinfo=pytz.timezone('UTC'))

    dt_local = dt.astimezone(pytz.timezone('Europe/Berlin'))
    return dt_local
"""