import hashlib
import os
import shutil
import subprocess
import re
import win32com.client

from creation_date import get_image_date, get_video_date
from image_location import get_image_location

def calculate_file_hash(file_path, hash_method='md5'):
    hash_obj = hashlib.new(hash_method)
    with open(file_path, 'rb') as file:
        for chunk in iter(lambda: file.read(4096), b""):
            hash_obj.update(chunk)
    return hash_obj.hexdigest()

def create_new_filename(date, location, file_hash, original_filename, file_extension):
    filename = ''
    if date:
        filename += date.strftime("%Y-%m-%d %H-%M-%S") if date else 'Unbekannte Zeit'
        filename += ' - '
    if location:
        filename += (location[0] + ' ' + location[1]) if location else 'Unbekannter Ort'
        filename += ' - ' 
    filename += original_filename + ' - ' + file_hash + file_extension
    return filename

def process_file(file_path, result_dir):
    original_filename = os.path.splitext(file_path)[0].split('\\')[-1]
    file_extension = os.path.splitext(file_path)[1].lower()
    if file_extension in ['.jpg', '.jpeg', '.png', '.heic', '.mp4', '.mov']:
        if file_extension in ['.mp4', '.mov']:
            date = get_video_date(file_path)
            location = None
        else:
            date = get_image_date(file_path)
            location = get_image_location(file_path)
        file_hash = calculate_file_hash(file_path)[:10]
        new_file_name = create_new_filename(date, location, file_hash, original_filename, file_extension)
        new_file_path = os.path.join(result_dir, new_file_name)

        if not os.path.exists(new_file_path):
            shutil.copy2(file_path, new_file_path)

def process_photos(source_dir, result_dir):
    print('processing photos...')

    def collect_file_paths(directory):
        file_paths = []
        for entry in os.listdir(directory):
            path = os.path.join(directory, entry)
            if os.path.isdir(path):
                file_paths.extend(collect_file_paths(path))
            else:
                file_paths.append(path)
        return file_paths

    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    file_paths = collect_file_paths(source_dir)

    for i, file_path in enumerate(file_paths):
        process_file(file_path, result_dir)
        print(f"Processing {i+1}/{len(file_paths)}: {file_path}")

current_working_directory = os.getcwd()

source_directory = os.path.join(current_working_directory, 'source')
result_directory = os.path.join(current_working_directory, 'result')

process_photos(source_directory, result_directory)
